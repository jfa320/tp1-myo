import os
from pyexpat import model

from pyscipopt import Model


def leer():
    # cargar y leer  el grafo txt

    leerTxt(input("Ingrese la ruta del archivo de texto:"))
    # crear txt  con variables y restricciones y guardarlo como zpl y cargar parametros del modelo

    print("Leyendo")



def optimizarPrincipal():
    print("-----------------")
    print(eliminarAristasDuplicadas(aristasCompletas))
    print(Costoaristas)
    print(PP)
    print(listaAristasObligatoria)
    print(FO)
    print("_---------------")
    mainOptimizar(eliminarAristasDuplicadas(aristasCompletas), Costoaristas, PP, listaAristasObligatoria, FO)

def imprimirResultado(RutaarchivoSalida,sol):
    try:
        RutaarchivoSalida += "Resultado" + NombreFileEntrada + ".txt"
        # Procesamiento para escribir en el fichero
        with open(RutaarchivoSalida, 'w') as f:
            for g in sol:
                f.write(g + '\n')
    except ValueError:
        print("Error no controlado." + ValueError)
    finally:
        f.close()

#C:/Users/Sebastian Font/Desktop/CiudadEjemplo1.txt
def armarFuncionObjetivo(g):
    aristas: str = g.replace('\n', "")
    aristas = g.split(",")
    rearmarFO= ""
    count=0
    for z in aristas:
        z.replace('\n', "")
        for x in z:
            if x.isnumeric():
                rearmarFO += x
        for v in z:
            if not v.isnumeric():
                rearmarFO += v
        rearmarFO += "+"
        count += 1
    rearmarFO = rearmarFO.rstrip(rearmarFO[-1])
    return rearmarFO

def completarAristas(h):
    listh = h.split(",")
    count = 0
    aristasnew = ""
    for d in listh:
        aristasnew += "," + d[1] + d[0]
        for f in d:
            if f.isnumeric():
                aristasnew += f
        count += 1
    return aristasnew


def generarAristas(z):
    VarOrigen = z[0]
    listaNodos.append(VarOrigen)
    z = z.replace(":", "")
    z = z.replace(" ", "")
    z = z.replace("\n", "")
    z = z.split(",")
    print(VarOrigen)
    aristasOriginal=""
    aristas = ""
    count = 0
    for s in z:
        if s[0] != VarOrigen:
            aristas += "," + VarOrigen + s
        else:
            aristas += s
        count += 1
    aristas += completarAristas(aristas)
    print("Aristas armadas:" + aristas)
    return aristas

def generarAristasOriginales(z):
    VarOrigen = z[0]
    z = z.replace(":", "")
    z = z.replace(" ", "")
    z = z.replace("\n", "")
    z = z.split(",")
    print(VarOrigen)
    aristasOriginal=""
    aristas = ""
    count = 0
    for s in z:
        if s[0] != VarOrigen:
            aristas += "," + VarOrigen + s
        else:
            aristas += s
        count += 1
    aristasOriginal+=aristas
    return aristasOriginal


def leerTxt(ruta_txt):
    try:
        archivo = open(ruta_txt, 'r')
        patharchivo = os.path.splitext(ruta_txt)
        contenido = archivo.readlines()
        global NombreFileEntrada
        NombreFileEntrada = patharchivo[0].split("/").pop()

        if contenido == '':  # validar si el archivo esta vacio
            print("El archivo seleccionado esta vacio por favor verifique su texto de entrada.")
        else:
            listaCaminos = []
            listaCaminosOriginales = []
            cont = 0
            global PP
            global listaNodos
            listaNodos = []
            for f in contenido:
                if not f.__contains__("#") and f != '\n' and f.__contains__(":"):

                    listaCaminos.append(generarAristas(f))

                    listaCaminosOriginales.append(generarAristasOriginales(f))

                elif not f.__contains__("#") and f != '\n':
                    cont += 1
                    if cont == 1:  # Punto de partida (PE)
                        PP = f.replace('\n',"")
                    elif cont > 1:  # aristas obligatorias
                        listaAristasObligatorias = f

            print("SE listan los caminos del grafo:")
            global FOOriginal
            FOOriginal = ""
            global FO
            FO = ""
            for x in listaCaminosOriginales:
                if FOOriginal != "":
                    FOOriginal = FOOriginal + "," + x
                else:
                    FOOriginal = FOOriginal + x
            print(FOOriginal)
            for x in listaCaminos:
                print(x)
                if FO != "":
                    FO = FO + "," + x
                else:
                    FO = FO + x

            global aristasCompletas
            aristasCompletas=[]

            global Costoaristas
            Costoaristas=[]

            # C:/Users/Sebastian Font/Desktop/CiudadEjemplo1.txt
            # FOOriginal = ArmarFuncionObjetivo(FOOriginal)
            # print("FO:" + FOOriginal)
            # FOOriginal = FOOriginal.replace('\n', "")
            # for s in FOOriginal.split("+"):
            #     costo = ""
            #     var=""
            #     for t in s:
            #         if t.isnumeric():
            #             costo += t
            #         else:
            #             var += t
            #     aristasCompletas.append(var)
            #     Costoaristas.append(int(costo))
            FO = armarFuncionObjetivo(FO)
            FO= FO.replace('\n', "")
            for s in FO.split("+"):
                costo = ""
                var=""
                for t in s:
                    if t.isnumeric():
                        costo += t
                    else:
                        var += t
                aristasCompletas.append(var)
                Costoaristas.append(int(costo))

            print("Punto de partida (PP)")
            print(PP)
            print("aristas obligatorias")
            global listaAristasObligatoria
            listaAristasObligatoria = listaAristasObligatorias.split(",")
            for g in listaAristasObligatoria:
                print(g.replace(" ", ""))


    except ValueError:
        print("Error no controlado." + ValueError)

    finally:
        archivo.close()

def productoArrays(arr1, arr2):
    arr3 = []
    i = 0
    while i < arr1.__len__():
        arr3.append(arr1.__getitem__(i) * arr2.__getitem__(i))
        i += 1
    return arr3


def buscarAristasOrigen(aristasOrigen, origen):
    array = []
    for arista in aristasOrigen:
        if arista.startswith(origen, 0, arista.__len__()):
            array.append(arista)
    return array


def buscarAristasDestino(aristasDestino, origen):
    array = []
    for arista in aristasDestino:
        if arista.endswith(origen, 0, arista.__len__()):
            array.append(arista)
    return array


def invertirString(string):
    return string[::-1]

# C:/Users/Sebastian Font/Desktop/CiudadEjemplo1.txt
def eliminarAristasDuplicadas(aristas):
    c = 0
    for g in aristas:
        c += 1
        i = 0
        for z in aristas:
            if g == z:
                i += 1
        if i > 1:
            aristas.remove(g)
            Costoaristas.pop(c)
    return aristas

def restriccionConservacion(aristas):
    varAristaConservacionFlujoRecibido=""
    varAristaConservacionFlujoEnviado=""
    strRestriccion=[]
    for nodo in listaNodos:
        for arista1 in aristas:
            if arista1.endswith(nodo):
                varAristaConservacionFlujoRecibido += arista1 + "+"
            elif arista1.startswith(nodo):
                varAristaConservacionFlujoEnviado += arista1 + "+"
        strRestriccion.append(varAristaConservacionFlujoRecibido[:-1]+"=="+varAristaConservacionFlujoEnviado[:-1])
        varAristaConservacionFlujoRecibido = ""
        varAristaConservacionFlujoEnviado = ""
    return strRestriccion

#MODELO
def mainOptimizar(aristas, Costoaristas, PP, aristasObligatorias, FO):
    print(aristas,Costoaristas,PP,aristasObligatorias,FO)
    # creo modelo
    print("Optimizando")
    # ----------------------------
    # datos hardcodeados BORRAR

    aristas = aristas # viene del txt
    costes = Costoaristas  # viene del txt
    origen = PP # PUNTO DE PARTIDA
    destino =  PP  # PUNTO DE FINALIZACION
    # ------------------------
    xij = []  # x: paso o no por arista ij | se cargara dinamicamente
    arrayAristasOrigen = buscarAristasOrigen(aristas, origen)
    arrayAristasDestino = buscarAristasDestino(aristas, destino)
    arrayAristasObligatorias = aristasObligatorias
    #
    model = Model("Censo")
    #
    # # defino variables

    for arista in aristas:  # cargo xij[]
     globals()[arista] = model.addVar(arista, vtype="INTEGER")
     xij.append(arista)

    # # defino funcion objetivo

    i = 0
    expresionFO=""
    while i < costes.__len__():
        if i == 0:
            expresionFO += costes.__getitem__(i).__str__() + "*" + xij.__getitem__(i)  # aca armo la sumatoria de: coste xij * xij
        else:
            expresionFO += "+" + costes.__getitem__(i).__str__() + "*" + xij.__getitem__(i)
        i += 1
    model.setObjective(eval(expresionFO), "minimize")  # eval convierte un string a una expresion

    # # defino restricciones
    #
    # # restriccion de origen
    expresionRestriccionOrigen = ""
    for arista in arrayAristasOrigen:
        expresionRestriccionOrigen += arista + "+"
    restriccionOrigen=eval(expresionRestriccionOrigen[:-1])
    model.addCons(restriccionOrigen >= 1)

    # restriccion de destino
    expresionRestriccionDestino = ""
    expresionRestriccionDestino2 = ""
    for arista in arrayAristasDestino:
        expresionRestriccionDestino += arista + "+"
    restriccionDestino = eval(expresionRestriccionDestino[:-1])
    model.addCons(restriccionDestino >=1)

    # # restriccion aristas obligatorias
    for arista in arrayAristasObligatorias:
        varAristaObligatoria = arista
        model.addCons(eval(varAristaObligatoria) >= 1)


    #RESTRICCION DESTINO C:/Users/Sebastian Font/Desktop/CiudadEjemplo1.txt
        # conservacion de flujo
        varAristaConservacionFlujo = ""
        #Armar lista de nodos
        varAristaConservacionFlujo = restriccionConservacion(aristas)
        for n in varAristaConservacionFlujo:
            resflujo = eval(n)
            model.addCons(resflujo)

    #optimizo
    model.optimize()
    # obtengo solucion
    sol = model.getBestSol()
    txtRes = ""
    if sol !="":
        # Se solicita la ruta del archivo resultado
        RutaarchivoSalida = input("Ingrese la ruta del archivo resultado de texto:")
        for x in xij:
            txtRes += x + ": {}".format(sol[eval(x)])+","
        # Imprimo el resultado en el txt resultado
        imprimirResultado(RutaarchivoSalida,txtRes.split(","))
