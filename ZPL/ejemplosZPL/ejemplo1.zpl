# Variable definitions
var x >= 0;
var y >= 0;

# Objective function
maximize obj: +143*x +60*y;

# Constraints
subto R1: +120*x +210*y <= 15000;
subto R2: +110*x +30*y <= 4000;
subto R3: +x +y <= 75;