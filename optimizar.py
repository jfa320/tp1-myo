from pyscipopt import Model

def productoArrays(arr1, arr2):
    arr3 = []
    i = 0
    while i < arr1.__len__():
        arr3.append(arr1.__getitem__(i) * arr2.__getitem__(i))
        i += 1
    return arr3


def buscarAristasOrigen(aristasOrigen, origen):
    array = []
    for arista in aristasOrigen:
        if arista.startswith(origen, 0, arista.__len__()):
            array.append(arista)
    return array


def buscarAristasDestino(aristasDestino, origen):
    array = []
    for arista in aristasDestino:
        if arista.endswith(origen, 0, arista.__len__()):
            array.append(arista)
    return array


def invertirString(string):
    return string[::-1]


def main_optimizar(aristasOrigin,origen,aristasObligatorias):
    # creo modelo
    print("Optimizando")
    # ----------------------------
    # datos hardcodeados BORRAR

    aristas = ["AB", "BA", "AC", "CA","AD", "DA"]  # viene del txt
    costes = [1, 1, 4, 4, 2, 2]  # viene del txt
    origen = "A"
    aristasObligatorias = ["AB","AD"] #la invertida se realiza al optimizar
    # ------------------------
    xij = []  # x: paso o no por arista ij | se cargara dinamicamente
    arrayAristasOrigen = buscarAristasOrigen(aristas, origen)
    arrayAristasDestino = buscarAristasDestino(aristas, origen)
    arrayAristasObligatorias = aristasObligatorias

    model = Model("Censo")

    # defino variables

    for arista in aristas:  # cargo xij[]
        globals()['x%s' % arista] = model.addVar("x: paso o no por %s" % arista, vtype="INTEGER")
        xij.append('x%s' % arista)

    # defino funcion objetivo

    expresionFO = ""
    i = 0
    while i < costes.__len__():
        expresionFO += "+" + costes.__getitem__(i).__str__() + "*" + xij.__getitem__(
            i)  # aca armo la sumatoria de: coste xij * xij
        i += 1

    model.setObjective(eval(expresionFO), "minimize")  # eval convierte un string a una expresion

    # defino restricciones

    # restriccion de origen
    expresionRestriccionOrigen = ""
    for arista in arrayAristasOrigen:
        expresionRestriccionOrigen += "x" + arista + "+"
    model.addCons(eval(expresionRestriccionOrigen[:-1]) >= 1)

    # restriccion de destino
    expresionRestriccionDestino = ""
    for arista in arrayAristasDestino:
        expresionRestriccionDestino += "x" + arista + "+"
    model.addCons(eval(expresionRestriccionDestino[:-1]) >= 1) # [:-1] elimina el ultimo caracter

    # restriccion aristas obligatorias

    for arista in arrayAristasObligatorias:
        varAristaObligatoria = "x" + arista + "+" + "x" + invertirString(arista)
        model.addCons(eval(varAristaObligatoria) >= 1)

    # conservacion de flujo


    for arista in aristas:
        if not arista.endswith(origen):
            nodoActual=arista.__getitem__(0)
            for arista1 in aristas:
                if arista1.endswith(nodoActual):
                    varAristaConservacionFlujoRecibido = "x" + arista1 +"+"
                elif arista1.startswith(nodoActual):
                    varAristaConservacionFlujoEnviado= "x"+ arista1 + "+"
            print("varAristaConservacionFlujoRecibido:{}",varAristaConservacionFlujoRecibido)
            print("varAristaConservacionFlujoEnviado:{}",varAristaConservacionFlujoEnviado)
            model.addCons(eval(varAristaConservacionFlujoRecibido[:-1]) == eval(varAristaConservacionFlujoEnviado[:-1]))

    #optimizo
    model.optimize()
    # obtengo solucion
    sol = model.getBestSol()

    for x in xij:
        print(x + ": {}".format(sol[eval(x)]))

    print("finalizo optimizar")


if __name__ == '__main__':
    main_optimizar("","","")
