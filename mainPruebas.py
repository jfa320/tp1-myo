# This is a sample Python script.

# Press Mayús+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from pyscipopt import Model


# Python3 program to find the shortest
# path between any two nodes using
# Floyd Warshall Algorithm.

# Initializing the distance and
# Next array
def initialise(V):
    global dis, Next

    for i in range(V):
        for j in range(V):
            dis[i][j] = graph[i][j]

            # No edge between node
            # i and j
            if (graph[i][j] == INF):
                Next[i][j] = -1
            else:
                Next[i][j] = j


# Function construct the shortest
# path between u and v
def constructPath(u, v):
    global graph, Next

    # If there's no path between
    # node u and v, simply return
    # an empty array
    if (Next[u][v] == -1):
        return {}

    # Storing the path in a vector
    path = [u]
    while (u != v):
        u = Next[u][v]
        path.append(u)

    return path


# Standard Floyd Warshall Algorithm
# with little modification Now if we find
# that dis[i][j] > dis[i][k] + dis[k][j]
# then we modify next[i][j] = next[i][k]
def floydWarshall(V):
    global dist, Next
    for k in range(V):
        for i in range(V):
            for j in range(V):

                # We cannot travel through
                # edge that doesn't exist
                if (dis[i][k] == INF or dis[k][j] == INF):
                    continue
                if (dis[i][j] > dis[i][k] + dis[k][j]):
                    dis[i][j] = dis[i][k] + dis[k][j]
                    Next[i][j] = Next[i][k]


# Print the shortest path
def printPath(path):
    n = len(path)

    for i in range(n - 1):
        print(path[i], end=" -> ")
    print(path[n - 1])

def distanciaMinima(nodo1,nodo2):
    return dis[nodo1][nodo2]

# Driver code
if __name__ == '__main__':
    MAXM, INF = 10, 10 ** 7
    dis = [[-1 for i in range(MAXM)] for i in range(MAXM)]
    Next = [[-1 for i in range(MAXM)] for i in range(MAXM)]

    V = 4
    graph = [[0, 3, INF, 7],
             [8, 0, 2, INF],
             [5, INF, 0, 1],
             [2, INF, INF, 0]]

    # Function to initialise the
    # distance and Next array
    initialise(V)

    # Calling Floyd Warshall Algorithm,
    # this will update the shortest
    # distance as well as Next array
    floydWarshall(V) #arma la matriz con distancias minimas
    path = []

    # Path from node 1 to 3
    print("Shortest path from 1 to 3: ", end="")
    print("Distancia camino minimo: "+ distanciaMinima(1,3).__str__())
    path = constructPath(1, 3) #armo el path en un array
    printPath(path) #le doy formato al array para imprimirlo

    # Path from node 0 to 2
    print("Shortest path from 0 to 2: ", end="")
    path = constructPath(0, 2)
    printPath(path)

    # Path from node 3 to 2
    print("Shortest path from 3 to 2: ", end="")
    path = constructPath(3, 2)
    printPath(path)



    # model = Model("Example")  # model name is optional
    # x = model.addVar("x", vtype="INTEGER")
    # y = model.addVar("y", vtype="INTEGER")
    #
    #
    # model.setObjective(143 * x + 60 * y, "maximize")
    # model.addCons(x >= 0)
    # model.addCons(y >= 0)
    # model.addCons(+120 * x + 210 * y <= 15000)
    # model.addCons(+110 * x + 30 * y <= 4000)
    # model.addCons(x + y <= 75)
    # model.addCons(x + y == 175)
    # model.optimize()
    # sol = model.getBestSol()
    # print(sol[x])
    # print("x: {}".format(sol[x]))
    # print("y: {}".format(sol[y]))
    #
    # arr1=[1,2,0]
    # arr2=[2,3,2]
    # arr3=[]
    # i=0
    # print(arr1.__len__())
    # while i < arr1.__len__():
    #     arr3.append(arr1.__getitem__(i)*arr2.__getitem__(i))
    #     i+=1
    #     print(i)
    # 
    # 
    # print(arr3)
    # arista="ABVasdas"
    # # print(arista.startswith('A', 0, arista.__len__()))
    #
    # xAD="De A a D"
    # xDA="De D a A"
    # xAB="De A a B"
    #
    # x=1
    # y=x.__str__()
    # print("pepe %s" %y)
    # xij=[xAD,xAB,xDA]
    #
